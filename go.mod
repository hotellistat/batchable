module batchable

go 1.15

require (
	github.com/cloudevents/sdk-go/v2 v2.4.1
	github.com/common-nighthawk/go-figure v0.0.0-20200609044655-c4b36f998cf2
	github.com/getsentry/sentry-go v0.10.0 // indirect
	github.com/jedib0t/go-pretty/v6 v6.1.0
	github.com/joho/godotenv v1.3.0
	github.com/prometheus/client_golang v1.10.0
	github.com/streadway/amqp v1.0.0
	golang.org/x/tools v0.0.0-20210106214847-113979e3529a // indirect
	k8s.io/apimachinery v0.20.2
)
